// Compte.cpp : Defines les class.
//

#include "particuliers.h"
#include "professionnels.h"
#include "Compte.h"

#include "MainFrm.h"
#include "CompteDoc.h"
#include "CompteApp.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

Public:
char SetNom(){ return Nom;};
char SetPrenom(){ return Prenom;};
char SetSexe(){ return Sexe;};
int GetTelephone()  { return Telephone;};
char SetAdresse ();
virtual ChargeTD();
char GetSituation()  { return Situation;}
void SetDateNaissance( int val) (DateNaissance = val;)
int GetSiret() { return Siret ;}
char  SetRaisonSociale (char val) ( SetRaisonSociale = val ;)
char  SetAnneeCreation (char val1) ( SetAnneeCreation = val1 ;)
char  SetAdresseEntreprise (char val2) ( SetAdresseEntreprise = val2 ;)
char  SetMail (char val3) ( SetAdresseEntreprise = val3 ;)

Privat:
Int numeroCompte;
Int DateOuverture;
Int Solde;
Int MontantDe
couvert; 



// CCompteApp

BEGIN_MESSAGE_MAP(CCompteApp, CWinApp)
	//{{AFX_MSG_MAP(CCompteApp)
	ON_COMMAND(ID_APP_ABOUT, OnAppAbout)
	// Standard file based document commands
	ON_COMMAND(ID_FILE_NEW, CWinApp::OnFileNew)
	ON_COMMAND(ID_FILE_OPEN, CWinApp::OnFileOpen)
END_MESSAGE_MAP()


// CCompteApp construction

CCompteApp::CCompteApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}


// The one and only CCompteApp object

CCompteApp theApp;


// CCompteApp initialization

BOOL CCompteApp::Initparticuliers()
{
	
#ifdef _AFXDLL
	Enable3dControls();		
#else
	Enable3dControlsStatic();	
#endif


	SetRegistryKey(_T("Local AppWizard-Generated Applications"));

	LoadStdProfileSettings();  // Load standard INI file options (including MRU)

	// Register the application's document templates.  Document templates
	//  serve as the connection between documents, frame windows and views.

	CSingleDocTemplate* pDocTemplate;
	pDocTemplate = new CSingleDocTemplate(
		IDR_MAINFRAME,
		RUNTIME_CLASS(Cinformation),
		RUNTIME_CLASS(Cclient),         
		RUNTIME_CLASS(Centreprise));
	AddDocTemplate(pDocTemplate);

	// Enable docOpen
	EnabledocOpen();
	RegisterdocOpen(TRUE);


	CCommandLineInfo cmdInfo;
	ParseCommandLine(cmdInfo);


	if (!ProcessShellCommand(cmdInfo))
		return FALSE;


	m_pMainWnd->ShowWindow(SW_SHOW);
	m_pMainWnd->UpdateWindow();

	m_pMainWnd->DragAcceptFiles();

	return TRUE;
}




class particuliers : public particuliers
{
public:
	particuliers();



	enum { IDD = IDD_ABOUTBOX };


	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support



protected:

	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{

}

void comptDoc::comptDoc(comptDoc* pDX)
{
	CDialog::comptDoc(pDX);

}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

// App command to run the dialog
void CCompteApp::OnAppAbout()
{
	CAboutDlg aboutDlg;
	aboutDlg.DoModal();
}

/////////////////////////////////////////////////////////////////////////////
// CCompteApp message handlers

